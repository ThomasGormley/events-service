package com.training.hackathon.events.controllers;

import com.training.hackathon.events.entities.Events;
import com.training.hackathon.events.services.EventsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/events")
public class EventsController {
    private static final Logger LOG = LoggerFactory.getLogger(EventsController.class);

    @Autowired
    private EventsService eventsService;

    @GetMapping
    public List<Events> findAll() {
        return eventsService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Events> findById(@PathVariable long id) {
        try {
            return new ResponseEntity<Events>(eventsService.findById(id), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<Events> create(@RequestBody Events events) {
        System.out.println(events.toString());
        try {
            return new ResponseEntity<Events>(eventsService.save(events), HttpStatus.CREATED);
        } catch (NoSuchElementException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
    @GetMapping("eventType/{eventType}")
    public ResponseEntity<List<Events>> findByEventType(@PathVariable String eventType) {
        try {
            return new ResponseEntity<List<Events>>(eventsService.findByEventType(eventType), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("tradeId/{tradeId}")
    public ResponseEntity<List<Events>> findByTradeId(@PathVariable int tradeId) {
        try {
            return new ResponseEntity<List<Events>>(eventsService.findByTradeId(tradeId), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/createdAt")
    public ResponseEntity<List<Events>> findBetweenCreatedAt(@RequestParam Date min, @RequestParam Date max) {
        try {
            return new ResponseEntity<List<Events>>(eventsService.getEventsByCreatedAtBetween(min, max), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }


}
