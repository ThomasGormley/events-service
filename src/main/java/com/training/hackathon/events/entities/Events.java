package com.training.hackathon.events.entities;

import com.fasterxml.jackson.annotation.JsonRawValue;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import com.vladmihalcea.hibernate.type.json.JsonType;
import org.apache.logging.log4j.util.StringMap;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "events")
@TypeDef(
        name = "json",
        typeClass = JsonStringType.class
)
public class Events {

    private java.util.Date date = new Date();

    // declare vars
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // the equivalent of auto increment - don't get dups
    @Column(name = "Event_ID")
    private Long eventID;

    @Column(name = "Trade_ID")
    private int tradeID;

    @Column(name = "Event_Type")
    private String eventType;

    @Column(name = "Created_At")
    private Date createdAt = new java.sql.Timestamp(date.getTime());

//    @Column(name = "Payload")
//    @Type(type = "json")
//    @JsonRawValue
//    private String payload;

    public Long getEventID() {
        return eventID;
    }

    // getters and setters
    public void setEventID(Long eventID) {
        this.eventID = eventID;
    }

    public int getTradeID() {
        return tradeID;
    }

    public void setTradeID(int tradeID) {
        this.tradeID = tradeID;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
//
//    public String getPayload() {
//        return payload;
//    }
//
//    public void setPayload(String payload) {
//        this.payload = payload;
//    }

    // methods

    @Override
    public String toString() {
        return "Events{" +
                "eventID=" + eventID +
                ", tradeID=" + tradeID +
                ", eventType='" + eventType + '\'' +
                ", createdAt=" + createdAt +
                '}';
    }
}

