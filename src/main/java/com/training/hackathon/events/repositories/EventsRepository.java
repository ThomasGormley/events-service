package com.training.hackathon.events.repositories;

import com.training.hackathon.events.entities.Events;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface EventsRepository extends JpaRepository<Events, Long> {

    List<Events> getAllByEventTypeEquals(String eventType);

    List<Events> getAllByTradeIDEquals(int tradeId);

    List<Events> getEventsByCreatedAtBetween(Date min, Date max);

}
