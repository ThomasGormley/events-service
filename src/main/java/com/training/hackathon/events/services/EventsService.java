package com.training.hackathon.events.services;

import com.training.hackathon.events.entities.Events;
import com.training.hackathon.events.repositories.EventsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class EventsService {

    @Autowired
    private EventsRepository eventsRepository;

    public List<Events> findAll() {
        return eventsRepository.findAll();
    }

    public Events findById(long id) {
        return eventsRepository.findById(id).get();
    }

    public Events save(Events events) {
        return eventsRepository.save(events);
    }

    public List<Events> findByEventType(String eventType) {
        return eventsRepository.getAllByEventTypeEquals(eventType);
    }

    public List<Events> findByTradeId(int tradeType) {
        return eventsRepository.getAllByTradeIDEquals(tradeType);
    }

    public List<Events> getEventsByCreatedAtBetween(Date min, Date max) {
        return eventsRepository.getEventsByCreatedAtBetween(min, max);
    }
}
