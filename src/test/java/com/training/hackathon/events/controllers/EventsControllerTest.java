package com.training.hackathon.events.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class EventsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void eventsControllerFindAllSuccess() throws Exception {

        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/events"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void eventsControllerFindAllNotFound() throws Exception {

        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/eventsz"))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andReturn();
    }

    @Test
    public void eventsControllerFindEventTypeSuccess() throws Exception {

        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/events/eventType/FULFILMENT_UPDATE"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void eventsControllerFindTradeIdSuccess() throws Exception {

        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/events/tradeId/60"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }





}
