package com.training.hackathon.events.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EventsTest {

    // constants
    private static final int testTradeId = 60;
    private static final String testEventType = "FULFILMENT_UPDATE";
    private static final String testPayload = "FROM: PROCESSING, TO: SUCCESS";

    private Events events;

    @BeforeEach
    public void setup() {
        this.events = new Events();
    }

    @Test
    public void setGetTradeIdTest() {
        this.events.setTradeID(testTradeId);
        assertEquals(this.events.getTradeID(), testTradeId);
    }

    @Test
    public void setGetEventTypeTest() {
        this.events.setEventType(testEventType);
        assertEquals(this.events.getEventType(), testEventType);
    }

//    @Test
//    public void setGetPayloadTest() {
//        this.events.setPayload(testPayload);
//        assertEquals(this.events.getPayload(), testPayload);
//    }


}
